# Desafio Labi9 Backend - Laravel


## Criar uma API Laravel de carrinho com autenticação e rotas protegidas

Desafio técnico criado para medir seus conhecimentos e habilidades de programação backend (Laravel).

A maior parte de tudo que é pedido neste desafio pode ser encontrado na documentação do [Laravel](https://laravel.com/docs)

Para documentar a aplicação recomendamos a utilização do [Elegan](https://gitlab.com/dev-in-commerce/elegan)


## Endpoints

Usuarios:

- Registrar

- Login

- Logout

Produtos (Apenas Usuários Logados) :

- Listar produtos

- Cadastrar produto

- Alterar produto

- Mostrar um produto

- Deletar produto

- Adicionar produto ao carrinho do usuário

- Remover produto ao carrinho do usuário

- Finalizar a Compra


## Alguns Models

User: 
(Campos padrões do Usuário Laravel)

Produtos:

- Nome *

- Preço *

- Quantidade *

- Descrição


\* Campos com (*) são obrigatorios

\*\* As demais models ficam a seu criterio os seus nomes e campos

## O que será avaliado:

- Qualidade e simplicidade do código.

- Utilização do padrão RESTful.

- Utilização dos recursos nativos do Laravel.

- Autenticação ([Sanctum](https://laravel.com/docs/sanctum) ou [JWT](https://jwt-auth.readthedocs.io/en/develop/laravel-installation/)).

- API bem documentada

- Lógica do carrinho de compras


## Diferencial

- Utilização de Request/Resource

- Tests cobrindo todos os pontos

- Utilização do inglês no codigo


## Aplicação

A aplicação é basicamente um carrinho onde o usuario poderá se registrar. Ao logar ele podera cadastrar produtos, listar, alterar e deleta-los.
O usuario tbm poderá vincular produtos ao seu carrinho e finalizar a compra.


## Importante

* **Lembrando que apesar de estar usando laravel a aplicação será uma api e não será necessario a utilização do blade ou qualquer parte visual** 


## Entrega

A aplicação deve ser criada em modo privado no seu repositorio github ou gitlab,
ao termino será necessario enviar um email para comercial@labi9.com junto dele enviar a collection do postman ou insomia e adicionar @jcsfran (caso utilizar github  adicionar [jcsfran](https://github.com/jcsfran)) ao repositorio.
